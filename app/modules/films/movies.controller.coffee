angular.module 'phonecat.movies'

.controller 'MoviesCtrl', ($scope, $http) ->
  $scope.kluczApi = '4e9220515669de70b15333498ed53bc0'
  $scope.wyniki = []
  $scope.glowna = ->
    $http.jsonp('https://api.themoviedb.org/3/discover/tv?first_air_date.gte=2014-08-01&sort_by=popularity.desc&api_key=' + $scope.kluczApi + '&callback=JSON_CALLBACK').success((dane) ->
      $scope.wyniki = dane.results
      return
    ).error (error) ->
      alert 'Houston, mamy problem z uzyskaniem danych.'
      return
    return
