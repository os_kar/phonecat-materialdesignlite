angular.module 'phonecat.movies', []

.config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state('root.movies',
      url: '/movies'
      templateUrl: 'modules/films/movies-list.html'
      controller: 'MoviesCtrl'
    )

