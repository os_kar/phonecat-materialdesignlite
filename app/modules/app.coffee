angular.module 'phonecat', ['ui.router', 'phonecat.phones', 'phonecat.movies']

.config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state('root',
      views:
        '':
          templateUrl: 'modules/navigation/navbar.html'
    )
