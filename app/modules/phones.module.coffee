angular.module 'phonecat.phones', []

.config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state('root.phones',
      url: '/phones'
      templateUrl: 'modules/phones/alloallo.html'
      controller: 'firstCtrl'
    )
  $urlRouterProvider.otherwise '/phones'

